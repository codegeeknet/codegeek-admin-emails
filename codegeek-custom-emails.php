<?php
/*
 Plugin Name: CodeGeek Custom Emails
 Plugin URI: https://www.codegeek.net/
 Description: Allows additional emails to be added to Standard Admin Email.
 Version: 1.0
 Author: CodeGeek
 Author URI: https://www.codegeek.net/
 License: GNU
 */

class CodeGeekCustomEmails
{
    public function __construct()
    {
        if(get_bloginfo('version') >= 4.9)
        {
            add_filter('admin_init' ,[$this,'register_fields']);
            add_filter('pre_update_option_codegeek_admin_emails', [$this, 'codegeek_sanitize_admin_emails_23'], 10, 2);
            add_filter('option_admin_email',[$this,'get_real_addresses']);
            add_action('update_option_codegeek_admin_emails',[$this,'remove_hashes'],10,3);
        }
        else
            add_filter('pre_update_option_admin_email',[$this,'codegeek_sanitize_admin_emails'],10,2);
    }

    public function remove_hashes($option,$old_value,$value)
    {
        delete_option( 'adminhash' );
        delete_option( 'new_admin_email' );
    }

    public function register_fields()
    {
        register_setting( 'general', 'codegeek_admin_emails');
        add_settings_field('codegeek_admin_emails', '<label for="codegeek_admin_emails">'.__('CodeGeek Admin Emails' , 'codegeek_admin_emails' ).'</label>' ,[$this,'fields_html'], 'general' );
    }

    public function fields_html()
    {
        $value = get_option( 'codegeek_admin_emails', '' );
        echo '<input type="text" id="codegeek_admin_emails" name="codegeek_admin_emails" class="regular-text ltr" value="' . $value . '" />';
        echo '<p class="multiple-admin-emails-description">This address overrides the Default Admin Email. Add one or more emails seperated by commas</p>';
    }

    public function get_real_addresses($value)
    {
        $multi = get_option('codegeek_admin_emails');

        if(strlen($multi) == 0)
            return $value;
        return $multi;
    }

    public function codegeek_sanitize_admin_emails_23($value,$oldValue)
    {
        $result = "";
        $emails = explode(",",$value);
        foreach($emails as $email)
        {
            $email = trim($email);
            $email = sanitize_email( $email );
            if(!is_email($email))
                return $oldValue;
            $result .= $email.",";

        }

        if(strlen($result == ""))
            return $oldValue;
        $result = substr($result,0,-1);

        return $result;
    }

    public function codegeek_sanitize_admin_emails($value,$oldValue)
    {
        if(!isset($_POST["admin_email"]))
            return $value;
        else
            $emails = $_POST["admin_email"];

        $result = "";
        $emails = explode(",",$emails);
        foreach($emails as $email)
        {
            $email = trim($email);
            $email = sanitize_email( $email );
            if(!is_email($email))
                return $value;
            $result .= $email.",";

        }

        if(strlen($result == ""))
            return $value;
        $result = substr($result,0,-1);

        return $result;
    }
}

$codeGeekCustomEmails = new CodeGeekCustomEmails();
