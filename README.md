=== CodeGeek Admin Emails ===

A simple solution for adding additional emails to the primary Admin email in WordPress.
 
== Description ==

CodeGeek Admin Emails is a solution for adding additional emails to the primary Admin email in WordPress.

<a href="https://www.codegeek.net/contact/" target="_blank">Contact Us</a>
== Installation ==
 
Activate the plugin through the 'Plugins' menu in WordPress.
Choose Settings -> General and set the "CodeGeek Admin Emails" field at the bottom of the page with comma separated list of emails.

== Frequently Asked Questions ==
 
= Will I lose my changes if I upgrade =
 
No. The data is stored safely in the database. Just remove the Standard Plugin and Upload the Pro version as you would any other plugin.

= How do I make changes after I have saved =
 
You need to make future changes in the "CodeGeek Admin Emails" field at the bottom of the page. If you wish to go back to a single email address, put that email in that field and click save. At that time, you can simply deactivate and remove the plugin.

== Changelog ==
